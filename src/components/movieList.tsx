import React, { useContext } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import { v4 as uuidv4 } from 'uuid';
import Button from '@material-ui/core/Button';
import { toast } from 'react-toastify';
import { MovieContext } from '../contexts/movieContext';
import { IMovie } from '../models/movie';
import { MovieService } from '../services/movieService';
import { MovieAction } from '../actions/movieActions';
import { ErrorBoundary } from './error/errorboundary';
import { ISearchMovieSuccessPayload } from '../actions/payloads/SearchMovieSuccessPayload';

export const MovieList = () => {
  const { state, dispatch } = useContext(MovieContext);
  const data: IMovie[] = state.searchResults;

  const loadMoreMovies = () => {
    MovieService.searchMovies(state.searchText, state.pageId + 1)
      .then((res) => {
        const moreData: IMovie[] = res.data.results;
        const payload: ISearchMovieSuccessPayload = {
          searchResults: [...data, ...moreData],
          searchText: state.searchText,
          pageId: res.data.page,
        };

        dispatch({
          type: MovieAction.SEARCH_MOVIES_SUCCESS,
          payload,
        });
      })
      .catch((error) => {
        toast.error('Something went wrong');

        dispatch({
          type: MovieAction.SEARCH_MOVIES_FAILED,
        });
      });
  };

  return (
    <div>
      {state.isLoading && (
        <div className="loader">
          <CircularProgress />
        </div>
      )}
      {state.searchResults.length === 0 ? (
        <div />
      ) : (
        <>
          <TableContainer component={Paper}>
            <Table className="table" aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell className="col-2">Title</TableCell>
                  <TableCell className="col-2">Release Date</TableCell>
                  <TableCell className="col-6">Overview</TableCell>
                  <TableCell className="col-2">Rating</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((row) => (
                  <ErrorBoundary key={uuidv4()}>
                    <TableRow key={uuidv4()}>
                      <TableCell>{row.title}</TableCell>
                      <TableCell>{row.release_date}</TableCell>
                      <TableCell>
                        {row.overview.length > 100
                          ? `${row.overview.substr(0, 100)}...`
                          : row.overview}
                      </TableCell>
                      <TableCell>{row.popularity}</TableCell>
                    </TableRow>
                  </ErrorBoundary>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <div className="load-more">
            <Button
              color="primary"
              variant="contained"
              onClick={() => loadMoreMovies()}
            >
              Load More
            </Button>
          </div>
        </>
      )}
    </div>
  );
};
