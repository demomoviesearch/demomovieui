import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { AuthContext } from '../contexts/appContext';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const AuthorizedRoute = (prop: any) => {
  const { state } = useContext(AuthContext);
  return (
    <Route
      render={() =>
        state.isAuthenticated ? (
          prop.children
        ) : (
          <Redirect to={{ pathname: '/login' }} />
        )}
    />
  );
};
