import React, { useContext } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { useHistory } from 'react-router-dom';
import { debounce } from 'lodash';
import { toast } from 'react-toastify';
import { AuthenticationService } from '../services/authenticationService';
import { MovieService } from '../services/movieService';
import { MovieAction } from '../actions/movieActions';
import { MovieContext } from '../contexts/movieContext';
import { IMovie } from '../models/movie';
import { ISearchMovieSuccessPayload } from '../actions/payloads/SearchMovieSuccessPayload';

export const Header = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const isMenuOpen = Boolean(anchorEl);
  const { dispatch } = useContext(MovieContext);
  const history = useHistory();

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    AuthenticationService.logout();
    history.push('/login');
  };

  const initiateSearch = (searchText: string) => {
    if (searchText && searchText.length >= 3) {
      dispatch({ type: MovieAction.SEARCH_MOVIES_LOADING });

      MovieService.searchMovies(searchText, 1)
        .then((res) => {
          const searchResults: IMovie[] = res.data.results;
          const payload: ISearchMovieSuccessPayload = {
            searchResults,
            searchText,
            pageId: res.data.page,
          };

          dispatch({
            type: MovieAction.SEARCH_MOVIES_SUCCESS,
            payload,
          });
        })
        .catch((err) => {
          toast.error('Something went wrong');

          dispatch({
            type: MovieAction.SEARCH_MOVIES_FAILED,
          });
        });
    }
  };

  const debounceFn = debounce((text) => {
    initiateSearch(text);
  }, 1000);

  const menuId = 'primary-search-account-menu';
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleLogout}>Logout</MenuItem>
    </Menu>
  );

  return (
    <div className="grow">
      <AppBar position="static">
        <Toolbar>
          <Typography className="title" variant="h6" noWrap>
            Search Movie
          </Typography>
          <div className="search">
            <div className="searchIcon">
              <SearchIcon />
            </div>
            <InputBase
              autoComplete="off"
              className="inputRoot inputInput"
              placeholder="Search…"
              id="searchBox"
              inputProps={{ 'aria-label': 'search' }}
              onChange={(e) => debounceFn(e.target.value)}
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              onKeyPress={(e: any) =>
                e.key === 'Enter' ? initiateSearch(e.target.value) : ''
              }
            />
          </div>
          <div className="grow" />
          <div className="sectionDesktop">
            <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>

      {renderMenu}
    </div>
  );
};
