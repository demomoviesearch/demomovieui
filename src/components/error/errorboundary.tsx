/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { Component } from 'react';

export class ErrorBoundary extends Component<any, ErrorState> {

  constructor(props: any) {
    super(props);
    this.state = { hasError: false, error: null, errorInfo: null };
  }

  static getDerivedStateFromError(error: any) {
    return { hasError: true };
  }

  componentDidCatch(error: any, errorInfo: any) {
    this.setState({ hasError: true, error, errorInfo });

    // We can log errors to any bug tracker service
  }

  render() {
    if (this.state.hasError) {
      return (
        <div><h1>Oops! Something went wrong!</h1>
          <p>The error: {this.state.error.toString()}</p>
          <p>Where it occured: {this.state.errorInfo.componentStack}</p>
        </div>);
    }

    return this.props.children;
  }
}

export interface ErrorState{
  hasError:boolean;
  error:any;
  errorInfo:any
}