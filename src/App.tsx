import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import './App.css';
import 'react-toastify/dist/ReactToastify.css';
import './styles/style.scss';
import { Login } from './pages/login';
import { AuthorizedRoute } from './components/authorizedRoute';
import { Home } from './pages/home';
import { AuthContextProvider } from './contexts/appContext';
import { MovieContextProvider } from './contexts/movieContext';

function App() {
  return (
    <>
      <AuthContextProvider>
        <MovieContextProvider>
          <Router>
            <Switch>
              <Route path="/login">
                <Login />
              </Route>
              <AuthorizedRoute path="/">
                <Home />
              </AuthorizedRoute>
            </Switch>
          </Router>
        </MovieContextProvider>
      </AuthContextProvider>
      <ToastContainer position={toast.POSITION.TOP_RIGHT} />
    </>
  );
}

export default App;
