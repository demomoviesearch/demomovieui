/* eslint-disable @typescript-eslint/no-explicit-any */
import axios, { AxiosRequestConfig, AxiosResponse, AxiosInstance } from 'axios';
import { RequestInterceptor } from '../interceptors/requestInterceptor';

export class FetchHttpClient {
  private static token: string;

  private static instance: AxiosInstance = axios.create({
    baseURL: 'http://localhost:8085/',
    timeout: 65000,
  });

  static post(
    url: string,
    isAuthenticatedRoot: boolean,
    data: any,
    config?: AxiosRequestConfig
  ): Promise<AxiosResponse> {
    return new Promise<AxiosResponse>((resolve: any, reject: any) => {
      const headers = config || {
        headers: { 'Content-Type': 'application/json' },
      };

      if (headers) {
        if (isAuthenticatedRoot)
          this.instance.interceptors.request.use(RequestInterceptor);
      }

      this.instance
        .post(url, data, headers)
        .then((response) => {
          resolve({ status: response.status, data: response.data });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  static put(
    url: string,
    isAuthenticatedRoot: boolean,
    data: any,
    config?: AxiosRequestConfig
  ): Promise<AxiosResponse> {
    return new Promise<AxiosResponse>((resolve: any, reject: any) => {
      const headers = config || {
        headers: { 'Content-Type': 'application/json' },
      };

      if (headers) {
        if (isAuthenticatedRoot)
          this.instance.interceptors.request.use(RequestInterceptor);
      }
      this.instance
        .put(url, data, headers)
        .then((response) => {
          resolve({ status: response.status, data: response.data });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  static get(
    url: string,
    isAuthenticatedRoot: boolean,
    config?: AxiosRequestConfig
  ): Promise<AxiosResponse> {
    return new Promise<AxiosResponse>((resolve: any, reject: any) => {
      const headers = config || {
        headers: { 'Content-Type': 'application/json' },
      };

      if (headers) {
        if (isAuthenticatedRoot)
          this.instance.interceptors.request.use(RequestInterceptor);
      }

      this.instance
        .get(url, headers)
        .then((response) => {
          resolve({ status: response.status, data: response.data });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }
}
