import { BehaviorSubject } from 'rxjs';
import { FetchHttpClient } from './fetch-service';
import { IUserCredentials } from '../models/credential';

const currentUserSubject = new BehaviorSubject(
  JSON.parse(localStorage.getItem('currentUser') || 'null')
);

export class AuthenticationService {
  static currentUser() {
    return currentUserSubject.asObservable();
  }

  static isAuthenticated(): boolean {
    if (currentUserSubject.value)
      return currentUserSubject.value.status === 200;

    return false;
  }

  static login(credential: IUserCredentials) {
    const body = {
      identifier: credential.userName,
      password: credential.passWord,
    };
    
    return FetchHttpClient.post('auth', false, body).then((user) => {
      localStorage.setItem('currentUser', JSON.stringify(user));
      currentUserSubject.next(user);
      return user;
    });
  }

  static logout() {
    localStorage.removeItem('currentUser');
    currentUserSubject.next(null);
  }
}
