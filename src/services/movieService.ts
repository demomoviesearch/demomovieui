import { FetchHttpClient } from './fetch-service';

export class MovieService {
  static searchMovies(searchText: string, pageId: number) {
    return FetchHttpClient.get(`searchMovies/${searchText}/${pageId}`, true);
  }
}
