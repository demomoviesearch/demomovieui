import { AxiosRequestConfig } from 'axios';

export const RequestInterceptor = (config: AxiosRequestConfig) => {
  const user = JSON.parse(localStorage.getItem('currentUser') || '');
  const axiosConfig = config;
  if (user.data.token) {
    axiosConfig.headers.Authorization = `Bearer ${user.data.token}`;
  }
  return axiosConfig;
};
