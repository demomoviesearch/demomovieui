import { ActionType } from '../models/actionType';
import { MovieAction } from '../actions/movieActions';
import { IMovieState } from '../models/movieState';

export const MovieReducer = (
  state: IMovieState,
  action: ActionType<IMovieState>
) => {
  switch (action.type) {
    case MovieAction.SEARCH_MOVIES_SUCCESS:
      return {
        isLoading: false,
        searchResults: action.payload.searchResults,
        searchText: action.payload.searchText,
        pageId: action.payload.pageId,
      };
    case MovieAction.SEARCH_MOVIES_LOADING:
      return { ...state, isLoading: true, searchResults: [] };
    case MovieAction.SEARCH_MOVIES_FAILED:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};
