import React, { createContext, useReducer } from 'react';
import { MovieReducer } from '../reducers/movieReducer';
import { IMovieContext } from '../models/movieContext';
import { IMovieState } from '../models/movieState';

export const MovieContext = createContext<IMovieContext>({} as IMovieContext);

const initialState: IMovieState = {
  searchResults: [],
  isLoading: false,
  searchText: '',
  pageId: 0,
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const MovieContextProvider = (props: any) => {
  const [state, dispatch] = useReducer(MovieReducer, initialState);

  return (
    <MovieContext.Provider value={{ state, dispatch }}>
      {props.children}
    </MovieContext.Provider>
  );
};
