/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { createContext, useState } from 'react';
import { AuthenticationService } from '../services/authenticationService';

export const AuthContext = createContext<any>({});
const currentUser = AuthenticationService.isAuthenticated();

const initialState = {
  isAuthenticated: currentUser,
};

export const AuthContextProvider = (props: any) => {
  const [state, dispatch] = useState(initialState);

  return (
    <AuthContext.Provider value={{ state, dispatch }}>
      {props.children}
    </AuthContext.Provider>
  );
};
