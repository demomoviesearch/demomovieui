/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useContext } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { AuthContext } from '../contexts/appContext';
import { AuthenticationService } from '../services/authenticationService';
import { IUserCredentials } from '../models/credential';

export const Login = () => {
  const { dispatch } = useContext(AuthContext);
  const history = useHistory();
  const credential: IUserCredentials = { userName: '', passWord: '' };
  const [userCredential, setCredential] = useState(credential);
  
  const handleLogin = (e: any) => {
    e.preventDefault();
    
    AuthenticationService.login(userCredential)
      .then((res) => {
        if (res.status === 200) {
          dispatch({ isAuthenticated: true });
          history.push('/');
        }
      })
      .catch((err) => {
        toast.error('Something went wrong');
      });
  };

  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: '100vh' }}
    >
      <Typography component="h1" variant="h5">
        Log in
      </Typography>
      <form onSubmit={(e) => handleLogin(e)}>
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          label="Email Address"
          onChange={(e) =>
            setCredential({ ...userCredential, userName: e.target.value })}
        />
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          label="Password"
          type="password"
          onChange={(e) =>
            setCredential({ ...userCredential, passWord: e.target.value })}
        />
        <Button type="submit" fullWidth variant="contained" color="primary">
          Log In
        </Button>
      </form>
    </Grid>
  );
};
