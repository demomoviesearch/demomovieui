import React from 'react';
import { MovieList } from '../components/movieList';
import { Header } from '../components/header';

export const Home = () => {
  return (
    <>
      <Header />
      <MovieList />
    </>
  );
};
