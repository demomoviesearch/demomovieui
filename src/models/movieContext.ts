import { IMovieState } from './movieState';

export interface IMovieContext {
  state: IMovieState;
  dispatch: any;
}
