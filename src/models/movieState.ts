import { IMovie } from './movie';

export interface IMovieState {
  searchResults: IMovie[];
  isLoading: boolean;
  searchText: string;
  pageId: number;
}
