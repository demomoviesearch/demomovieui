export interface IMovie {
  title: string;
  release_date: string;
  overview: string;
  popularity: number;
}
