import { IMovie } from '../../models/movie';

export interface ISearchMovieSuccessPayload {
  searchResults: IMovie[];
  searchText: string;
  pageId: number;
}
