# Demo Movie Search - UI

UI is completely built on react which includes the usage of react, typescript, context API with hooks.

## Requirement

* Node
* NPM

## Installation

Clone [UI](https://gitlab.com/demomoviesearch/demomovieui) to your environment.
and run the following commands

`npm install`

then

`npm start`

## Credentials

User name : `testuser`
Password : `test123`

## Technologies and libraries

* React
* Typescript
* Context API
* Hooks
* Material UI
* Prettier
* Axios
* rxjs
* ESlint


